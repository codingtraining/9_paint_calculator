package app.com.study.stickyny.paintcalculator;

import android.support.v4.view.animation.PathInterpolatorCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
    private static final int PAINT_AMOUNT = 9;
    TextView mResultText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText widthEdit = (EditText) findViewById(R.id.width_edit);
        final EditText lengthEdit = (EditText) findViewById(R.id.length_edit);
        mResultText = (TextView) findViewById(R.id.result_text);

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!widthEdit.getText().toString().isEmpty() && !lengthEdit.getText().toString().isEmpty()) {
                    print(Integer.parseInt(widthEdit.getText().toString()),
                            Integer.parseInt(lengthEdit.getText().toString()));
                }
            }
        };

        widthEdit.addTextChangedListener(textWatcher);
        lengthEdit.addTextChangedListener(textWatcher);
    }
    private void print(int width, int length) {
        mResultText.setText(getString(R.string.result_text, calculator(width, length)));
    }

    private int calculator(int width, int length) {
        int num = (width * length) / PAINT_AMOUNT;
        int rest = (width * length) % PAINT_AMOUNT;

        if (rest > 0) {
            return num + 1;
        } else {
            return num;
        }
    }
}
